// The following are for the ltl properties defined in properties.prop
#define patient_pos_covid (doctor_f@Patient_pos)
#define patient_exam (doctor_f@Exam_Start)
#define patient_dischargedd (doctor_f@end_61)
#define hospital_care_appropriate (doctor_f@end_47)

// --------------------------------------------------------------------
byte severity = 0 
byte next_exam = 0

byte num_analyses  = 0 
byte days = 0 
byte normal_signs_time = 0
byte days_normal_signs = 0 
byte days_partial = 0
bool normal_signs_started = false

chan analysis_to_doctor = [2] of { bool, bool }
chan doctor_to_homecare = [1] of { byte }
chan homecare_to_doctor = [0] of { byte }

mtype = { sync }
mtype = { phware_ordered, patient_admitted, patient_discharged }
mtype = { severe_signs, mild_signs, normal_signs }
mtype = { urgent_exam, routine_exam, no_exam }

#define byte_max 255
#define time_slice 6

#define fourteen_days_passed (days >= 14)
#define normal_signs_discharge (fourteen_days_passed && days_normal_signs >= 3) 
#define five_days  5 * (24/time_slice)
#define one_day (24/time_slice)

active [1] proctype doctor_f() {
	printf("doctor_f\n")
Patient_pos: 
	printf("Patient test + for COVID19\n")
	byte patient = 0
    byte orders 
	goto Exam_Start 
Exam_loop:
	homecare_to_doctor?patient
Exam_Start: // patient exam
	atomic {
		printf("01-Doctor or Nurse examine patient\n")
		num_analyses = 0 
		if
			:: normal_signs_started != true -> 
				severity = severe_signs
			:: normal_signs_started != true ->
				severity = mild_signs
			:: fourteen_days_passed -> 
				severity = normal_signs 
				normal_signs_started = true
		fi
		if
			:: severity == severe_signs ->
				orders = patient_admitted
				printf("Doctor admitted patient\n")
			:: severity == normal_signs -> 
				if
					:: normal_signs_discharge -> 
						orders = patient_discharged
						printf("14 + 3 days normal signs\n")
					:: else -> orders = phware_ordered 
				fi
			:: severity == mild_signs -> 
				orders = phware_ordered
				printf(" signs Doctor ordered phware\n")
		fi
		if 
			:: (orders == phware_ordered) -> 
Homecare_appropriate:
				printf("02 Doctor orders home care with PHWARE and 4-7 day routine exam\n")
				next_exam = five_days
				run homecare_f()
			:: (orders == patient_admitted) ->
				printf("Doctor admitted patient for hospital or ICU care\n")
				goto end_47
			:: (orders == patient_discharged) ->
				printf("Patient discharged\n")
				goto end_61 		
		fi
	}
	byte exam_type = no_exam
	bool alert 
	bool trend_gt_cap
Catch_AI_Packet: 
	atomic {
        printf("Seconds catch AI packet next_exam  = %d, num_analyses = %d\n", next_exam, num_analyses)
		if
            :: nfull(analysis_to_doctor) ->
                if
                    :: true -> goto Catch_AI_Packet
                    :: true -> skip 
                fi
            :: full(analysis_to_doctor) -> skip
        fi
        do
            :: analysis_to_doctor?[alert,trend_gt_cap] ->
                analysis_to_doctor?alert,trend_gt_cap
				printf("Doctor received analysis, alert = %d trend_gt_cap = %d\n", alert, trend_gt_cap)
            :: else -> break
        od
        if
			:: next_exam == num_analyses -> 
                goto Exam_loop 
			:: else -> skip
		fi
		printf("Doc-Nurse review alert/vitals trend\n")
		if
			:: alert == true ->
				if
					:: true ->
						printf("Alert, Doctor orders urgent exam\n")
						exam_type = urgent_exam 
					:: true ->
						printf("Alert, Doctor orders no exam\n") 
						exam_type = no_exam 
				fi
		    :: alert == false ->
				if
					:: trend_gt_cap ->
                    	printf("No alert, Doctor orders urgent exam\n")
                        exam_type = urgent_exam
					:: true -> 
                        exam_type = no_exam
						printf("No alert, Doctor orders no exam\n") 
				fi
		fi
		if
			:: exam_type == urgent_exam -> 		
				next_exam = num_analyses + 1 
			:: exam_type == no_exam -> skip
		fi
		goto Catch_AI_Packet 
	}
end_61:
end_47: 	
	printf("end doctor_f\n")
}

proctype homecare_f() {
	atomic {
		printf("homecare_f\n")
Start_Homecare:
		printf("Get Phware & install app\n")
		bool alert
		bool trend_gt_cap
Check_next_exam:
		printf("Homecare, next_exam = %d\n", next_exam)
		if 
			:: next_exam == num_analyses  ->
				if
					:: (num_analyses + days_partial) % one_day != 0
						days_partial = (num_analyses + days_partial) % one_day
					:: else -> skip 
				fi
				goto Prep_Exam
			:: else -> skip 
		fi
		if
			:: true ->
				alert = true
			:: true ->
				alert = false
		fi
		if
			:: true ->
				trend_gt_cap = true
			:: true -> 
				trend_gt_cap = false
		fi
		printf("Sending analysis to doctor\n")
		analysis_to_doctor!alert,trend_gt_cap
		num_analyses = num_analyses + 1 
		if 
			:: (num_analyses + days_partial) % one_day == 0 ->
				if
					:: normal_signs_started != true  
						days = days + 1
					:: normal_signs_started == true 
						days_normal_signs = days_normal_signs + 1
				fi
			:: else -> skip
		fi
		goto Check_next_exam
Prep_Exam:
		printf("07- Patient prep exam\n")
		homecare_to_doctor!sync 
	}
end_30:
	printf("end homecare_f\n")
}