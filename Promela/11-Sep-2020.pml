mtype = {levelPhware, levelICU, levelNormal}
mtype = {trendBad, trendGood}
mtype = {ptExpired, end61}
mtype = {now, later}

mtype webExamTime = now
mtype patientStatus = levelNormal

chan heartBeat = [2] of {bool}
chan examTime = [1] of {bool}
chan vitals = [2] of {bool, mtype}

mtype = {ExamState, PtInAppropriateHomeCareStatus, PtDischargedState, ReviewedState, 
    PtAtIncreasedRiskInHomeCareState, PtExpiredState, HospitalOrICUCareAppropriateState, InitState}

byte state = InitState

inline setLevel(level) {
  if
  :: skip -> level = levelPhware
  :: skip -> level = levelICU
  :: skip -> level = levelNormal
  fi
}

inline setWebExamTimeNowPlus12to24(examTime) {
  if
  :: skip -> examTime = now
  :: skip -> examTime = later
  fi
}

inline clearVitals(vitals) {
  if
  :: nempty(vitals) -> vitals?_,_
  :: empty(vitals) -> skip
  fi
}

inline setState(newState) {
  state = newState
}

proctype clinician() {
  bool alert = false
  mtype trend = trendGood
patientExam:
  atomic {
    examTime?_
    clearVitals(vitals)
    printf("01- Doctor or Nurse examine pt\n")
    setLevel(patientStatus)
    setState(ExamState)
  }
  if
  :: patientStatus == levelNormal -> 
    atomic {
      patientStatus = end61
      setState(PtExpiredState)
      goto end
    }
  :: patientStatus == levelICU -> 
    atomic {
      printf("Doctor admit pt for hospital or ICU care\n")
      setState(HospitalOrICUCareAppropriateState)
    }
    atomic {
      if 
      :: skip -> 
          patientStatus = ptExpired
          setState(PtExpiredState)
          goto end
      :: skip -> examTime!true
      fi
      goto patientExam
    }
  :: patientStatus == levelPhware ->
    atomic {
      printf("02- Doctor orders home care with PHWARE and 4-7 day routine exam\n")
      webExamTime = later
      run homeCareFlow()
    }
  fi

waitVitals:
  atomic {
    setState(PtInAppropriateHomeCareStatus)
    vitals?alert, trend
    if
    :: alert ->
         goto review
    :: else -> 
         goto exam
    fi
  }

review:
  atomic {
    printf("05a- Doc-Nurse review alert\n")
    setState(ReviewedState)
    if 
    :: skip ->
         goto waitVitals
    :: skip ->
         goto scheduleNow
    fi
  }

exam:
  atomic {
    printf("05b- Doc-Nurse review vitals trend\n")
    setState(ReviewedState)
    if
    :: trend == trendBad ->
      goto scheduleNow
    :: trend == trendGood ->
         printf("06b- Scheduler sets up webExamTime=now+12, 24 hours\n")
         setWebExamTimeNowPlus12to24(webExamTime)
         goto anyExamNow
    fi
  }

scheduleNow:
  atomic {
    printf("06a- Scheduler sets up webExamTime=now\n")
    webExamTime = now
    goto anyExamNow
  }

anyExamNow:
  atomic {
    if
    :: webExamTime == now ->
         goto patientExam
    :: else ->
         goto waitVitals
    fi
  }
end:
}

inline sendVitals(vitals) {
  if
  :: skip -> 
    vitals!true, trendBad
    setState(PtAtIncreasedRiskInHomeCareState)
  :: skip -> 
    vitals!false, trendBad
    setState(PtAtIncreasedRiskInHomeCareState)
  :: skip -> 
    vitals!false, trendGood
  fi
}

proctype homeCareFlow() {
  printf("Get Phware & install app\n")
loop:
  atomic {
    heartBeat?_
    printf("03- Pt-caregiver follow order to record vitals\n")
    sendVitals(vitals)
  }
  atomic {
    if
    :: webExamTime == now ->
         printf("07- Pt prep for exam\n")
         examTime!true
         goto end
    :: else -> 
         goto loop
    fi
  }
end:  
}

#define isEnd (patientStatus == ptExpired || patientStatus == end61)

init {
  atomic {
    run clinician()
    examTime!true
  }
loop:
  atomic {
    (timeout)
    if
    :: isEnd -> goto end
    :: else -> heartBeat!true
               goto loop
    fi
  end:
  }
}

#define fair (always (eventually ((patientStatus == levelNormal) || (patientStatus == ptExpired))))

/**
 * The patient evenually expires or no longer needs any care
 */
ltl eventuallyExpiresOrNoCareNeeded {fair -> (eventually (patientStatus == ptExpired || patientStatus == end61))}

/**
 * Exam state exists in model
 */
ltl examExists {fair -> (eventually (webExamTime == now))}
