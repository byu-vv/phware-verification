#!/bin/bash
# This script reads in ltl properties from a file, with a name for the property on the line preceding it

function usage {
	echo "There are two required arguments:"
	echo "	-p file.pml : the promela file to run ltl properties on"
	echo "	-t file : the file with ltl properties to run against a promela model"
	echo ""	
	echo "Three optional arguments can be provided:"
	echo "	-o output_dir : use a different output directory than the default ./output"
	echo "	-m N : will be used as a spin argument of the same name"
	echo "	-f filename : name the output file generated rather than the default"
	exit 1
}

PROMFILE=NULL
PROPFILE=NULL
OUTDIR=./output
OUTFILE=NULL

SPINDEPTH=10000

while getopts ":p:t:o:m:f:?" opt; do
	case ${opt} in
		o) OUTDIR=$OPTARG
			;;
		m) SPINDEPTH=$OPTARG
			;;
		p) PROMFILE=$OPTARG
			;;
	 	t) PROPFILE=$OPTARG
	    	;;
		f) OUTFILE=$OPTARG
			;; 
		:) echo "Invalid usage"
			usage
			;;
	   	?) usage
			;; 

    	esac
done
shift $((OPTIND -1))

if [ ! "$PROMFILE" ] || [ ! "$PROPFILE" ]; then
	usage
fi

if [ ! -f "$PROPFILE" ]; then 
	echo "Properties file ${PROPFILE} not found"
	exit
fi

if [ ! -f "$PROMFILE" ]; then
	echo "Promela file ${PROMFILE} not found"
	exit
fi

if [ ! -d "$OUTDIR" ]; then
	echo "Creating ${OUTDIR}"
	mkdir -p "$OUTDIR" 
fi

if [ -z "$OUTFILE" ]; then 
	MONTH=$(date +"%m")
	DAY=$(date +"%d")
	YEAR=$(date +"%Y")
	TIME=$(date +"%H%M")
	OUTFILE="${OUTDIR}/${PROMFILE%.*}_${YEAR}_${MONTH}_${DAY}_${TIME}"
else
	OUTFILE="${OUTDIR}/$OUTFILE"
fi

rm -rf "$OUTFILE"

touch "$OUTFILE" 

echo "Running ltl properties in ${PROPFILE}" 
echo "Output will be located in ${OUTFILE}" 

while IFS= read -r line; do
	if [[ $line == "ltl"* ]]; then
		line=${line:4}
		echo "${line}" 
		echo ""
		spin -f "$line" -run -m"$SPINDEPTH" "$PROMFILE" >> "$OUTFILE"		
		echo "" >> "$OUTFILE"
	elif [[ $line == "#"* || -z $line ]]; then :  
	else
		echo "Running property ${line}"
		echo "${line}" >> "$OUTFILE"
	fi 	
done < "$PROPFILE"
