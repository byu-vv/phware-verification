chan heartBeat = [2] of {bool}

/*****************************************************************************/
/*                         Clinician Flow Places                             */
/*****************************************************************************/
bit clinicianEnd61 = 0
bit clinicianEndPtExpired = 0
bit clinicianGate00In0 = 0
bit clinicianGate00In1 = 0
bit clinicianGate00In2 = 0
bit clinicianGate01In0 = 0
bit clinicianGate01In1 = 0
bit clinicianGate02In0 = 0
bit clinicianGate02In1 = 0
bit clinicianGate02In2 = 0
bit clinicianGate03 = 0
bit clinicianGate04 = 0
bit clinicianGate05 = 0
bit clinicianGate06 = 0
bit clinicianGate07 = 0
bit clinicianGate08 = 0
bit clinicianGate09In0 = 0
bit clinicianGate09In1 = 0
bit clinicianGate09In2 = 0
bit clinicianGate09In3 = 0
bit clinicianGate10In0 = 0
bit clinicianGate10In1 = 0
bit clinicianGate11In0 = 0
bit clinicianGate11In1 = 0
bit clinicianRecv00 = 0
bit clinicianRecv01 = 0
bit clinicianTask01 = 0
bit clinicianTask02 = 0
bit clinicianTask03 = 0
bit clinicianTask05a = 0
bit clinicianTask05b = 0
bit clinicianTask06a = 0
bit clinicianTask06b = 0
bit clinicianWait00 = 0

/*****************************************************************************/
/*                         Home Care Flow Places                             */
/*****************************************************************************/
bit homeCareFlowEnd30 = 0
bit homeCareFlowGate00In0 = 0
bit homeCareFlowGate00In1 = 0
bit homeCareFlowGate01 = 0
bit homeCareFlowTask00 = 0
bit homeCareFlowTask03 = 0
bit homeCareFlowTask07 = 0
bit homeCareFlowSend00 = 0
bit homeCareFlowRecv00 = 0
bit homeCareFlowWait00 = 0

/*****************************************************************************/
/*                             Token Management                              */
/*****************************************************************************/
#define hasToken(place) (place != 0)

#define putToken(place) place = 1

#define consumeToken(place) place = 0

#define hasToken2Xor(place0, place1) \
  ((place0 != 0 && place1 == 0) || (place0 == 0 && place1 != 0))


#define hasToken3Xor(place0, place1, place2)         \
  (                                                  \
       (place0 != 0 && place1 == 0 && place2 == 0)   \
    || (place0 == 0 && place1 != 0 && place2 == 0)   \
    || (place0 == 0 && place1 == 0 && place2 != 0)   \
  )

#define hasToken4Xor(place0, place1, place2, place3)                \
  (                                                                 \
       (place0 != 0 && place1 == 0 && place2 == 0 && place3 == 0)   \
    || (place0 == 0 && place1 != 0 && place2 == 0 && place3 == 0)   \
    || (place0 == 0 && place1 == 0 && place2 != 0 && place3 == 0)   \
    || (place0 == 0 && place1 == 0 && place2 == 0 && place3 != 0)   \
  )

#define hasToken2And(place0, place1) (place0 != 0 && place1 != 0)

/*****************************************************************************/
/*                            Patient Status                                 */
/*****************************************************************************/
mtype = {levelPhware, levelICU, levelNormal, levelExpired}
mtype = {trendBad, trendGood}

mtype patientStatus = levelNormal
bool alert = false
mtype trend = trendGood

#define setTrendGood(trend) trend = trendGood

#define setTrendBad(trend) trend = trendBad

#define setAlert(alert) alert = true

#define resetAlert(alert) alert = false

#define isAlert(alert) (alert == true)

inline setICUOrNormalOrPhWareAndMatchTrend(level, trend) {
  if
  :: skip -> 
    level = levelPhware
  :: skip -> 
    level = levelICU
    setTrendBad(trend)
  :: skip -> 
    level = levelNormal
    setTrendGood(trend)
  fi
}

inline setExpiredOrNormal(level) {
  if
  :: skip -> level = levelExpired
  :: skip -> level = levelNormal
  fi
}

#define isIcu(level) (level == levelICU)

#define isNormal(level) (level == levelNormal)

#define isPhware(level) (level == levelPhware)

#define isExpired(level) (level == levelExpired)

#define isTrendGood(trend) (trend == trendGood)

#define isTrendBad(trend) (trend == trendBad)

inline setAndSendVitals(alert, trend) {
  if
  :: (isAlert(alert) || isTrendBad(trend)) ->
  :: else -> 
    if
    :: skip -> 
      setAlert(alert)
      setTrendBad(trend)
    :: skip -> 
      resetAlert(alert)
      setTrendBad(trend)
    :: skip -> 
      resetAlert(alert)
      setTrendGood(trend)
    fi
  fi
}

/*****************************************************************************/
/*                                  Exam                                     */
/*****************************************************************************/
mtype = {routine, urgent}
mtype = {now, unscheduled, scheduled}

mtype examType = routine
mtype examTime = unscheduled

#define setExamTypeUrgent(type) type = urgent

#define isExamTypeUrgent(type) (type == urgent)

#define setExamTypeRoutine(type) type = routine

#define isExamTypeRoutine(type) (type == routine)

#define isExamTimeNow(time) (time == now)

#define setExamTimeNow(time) time = now

#define isExamTimeScheduled(time) (time == scheduled)

#define setExamTimeScheduled(time) time = scheduled

#define isExamTimeUnscheduled(time) (time == unscheduled)

#define setExamTimeUnscheduled(time) time = unscheduled

inline clearAlertAndSetExamTypeBasedOnTrend(alert, type, trend) {
  resetAlert(alert)
  if
  :: (isExamTypeRoutine(type) && isTrendBad(trend)) ->
    setExamTypeUrgent(type)
  :: (isExamTypeRoutine(type) && isTrendBad(trend)) ->
    setTrendGood(trend)
  :: else
  fi
}

inline setExamTimeIfScheduled(time) {
  if
  :: isExamTimeScheduled(time) ->
    setExamTimeNow(time)
  :: true
  fi
}

proctype clinician() {
  putToken(clinicianGate00In2)
  putToken(clinicianGate02In1)
  do
  :: hasToken3Xor(clinicianGate00In0, clinicianGate00In1, clinicianGate00In2) ->
    atomic {
      consumeToken(clinicianGate00In0)
      consumeToken(clinicianGate00In1)
      consumeToken(clinicianGate00In2)
      putToken(clinicianGate01In1)
    }
  :: hasToken2And(clinicianGate01In0, clinicianGate01In1) ->
    atomic {
      consumeToken(clinicianGate01In0)
      consumeToken(clinicianGate01In1)
      putToken(clinicianTask01)
    }
  :: hasToken3Xor(clinicianGate02In0, clinicianGate02In1, clinicianGate02In2) ->
    atomic {
      consumeToken(clinicianGate02In0)
      consumeToken(clinicianGate02In1)
      consumeToken(clinicianGate02In2)
      putToken(clinicianGate01In0)
    }
  :: hasToken(clinicianGate03) ->
    atomic {
      consumeToken(clinicianGate03)
      if
      :: isIcu(patientStatus) ->
        putToken(clinicianTask03)
      :: isNormal(patientStatus) ->
        putToken(clinicianEnd61)
        break
      :: isPhware(patientStatus) ->
        putToken(clinicianTask02)
      fi
    }
  :: hasToken(clinicianGate04) ->
    atomic {
      consumeToken(clinicianGate04)
      if
      :: isExpired(patientStatus) ->
        putToken(clinicianEndPtExpired)
        break
      :: isNormal(patientStatus) ->
        putToken(clinicianGate05)
      fi
    }
  :: hasToken(clinicianGate05) ->
    atomic {
      consumeToken(clinicianGate05)
      putToken(clinicianGate02In2)
      putToken(clinicianGate00In0)
    }
  :: hasToken(clinicianGate06) ->
    atomic {
      consumeToken(clinicianGate06)
      if
      :: isAlert(alert) ->
        putToken(clinicianTask05a)
      :: else ->
        putToken(clinicianWait00)
      fi
    }
  :: hasToken(clinicianGate07) ->
    atomic {
      consumeToken(clinicianGate07)
      if 
      :: isExamTimeNow(examTime) ->
        putToken(clinicianGate09In3)
      :: else ->
        if
        :: isExamTypeUrgent(examType) ->
          putToken(clinicianGate10In1)
        :: isExamTypeRoutine(examType) && isExamTimeUnscheduled(examTime) ->
          putToken(clinicianGate11In0)
        :: else ->
          putToken(clinicianRecv01)
        fi
      fi
    }
  :: hasToken(clinicianGate08) ->
    atomic {
      consumeToken(clinicianGate08)
      if 
      :: isExamTimeNow(examTime) ->
        putToken(clinicianGate09In0)
      :: else ->
        if
        :: isExamTypeUrgent(examType) ->
          putToken(clinicianGate10In0)
        :: isExamTypeRoutine(examType) && isExamTimeUnscheduled(examTime) ->
          putToken(clinicianGate11In1)
        :: else ->
          putToken(clinicianRecv01)
        fi
      fi
    }
  :: hasToken4Xor(clinicianGate09In0, clinicianGate09In1, clinicianGate09In2, clinicianGate09In3) ->
    atomic {
      consumeToken(clinicianGate09In0)
      consumeToken(clinicianGate09In1)
      consumeToken(clinicianGate09In2)
      consumeToken(clinicianGate09In3)
      if
      :: isExamTimeNow(examTime) ->
        putToken(clinicianGate02In0)
      :: else -> 
        putToken(clinicianRecv01)
      fi
    }
  :: hasToken2Xor(clinicianGate10In0, clinicianGate10In1) ->
    atomic {
      consumeToken(clinicianGate10In0)
      consumeToken(clinicianGate10In1)
      putToken(clinicianTask06a)
    }
  :: hasToken2Xor(clinicianGate11In0, clinicianGate11In1) ->
    atomic {
      consumeToken(clinicianGate11In0)
      consumeToken(clinicianGate11In1)
      putToken(clinicianTask06b)
    }
  :: hasToken(clinicianRecv00) ->
    atomic {
      consumeToken(clinicianRecv00)
      putToken(clinicianGate00In1)
    }
   :: hasToken(clinicianRecv01) ->
    atomic {
      consumeToken(clinicianRecv01)
      putToken(clinicianGate06)
    }
  :: hasToken(clinicianTask01) ->
    atomic {
      consumeToken(clinicianTask01)
      printf("01- Doctor or Nurse examine pt\n")
      setExamTimeUnscheduled(examTime)
      setExamTypeRoutine(examType)
      setICUOrNormalOrPhWareAndMatchTrend(patientStatus, trend)
      putToken(clinicianGate03)
    }
  :: hasToken(clinicianTask02) ->
    atomic {
      consumeToken(clinicianTask02)
      printf("02- Doctor orders home care with PHWARE and 4-7 day routine exam\n")
      setExamTypeRoutine(examType)
      putToken(clinicianRecv01)
      putToken(homeCareFlowRecv00)
    }
  :: hasToken(clinicianTask03) ->
    atomic {
      consumeToken(clinicianTask03)
      printf("Doctor admit pt for hospital or ICU care\n")
      setExpiredOrNormal(patientStatus)
      putToken(clinicianGate04)
    }
  :: hasToken(clinicianTask05a) ->
    atomic {
      consumeToken(clinicianTask05a)
      printf("05a- Doc-Nurse review alert, vitals and exam schedule\n")
      clearAlertAndSetExamTypeBasedOnTrend(alert, examType, trend)
      setExamTimeIfScheduled(examTime)
      putToken(clinicianGate07)
    }
  :: hasToken(clinicianTask05b) ->
    atomic {
      consumeToken(clinicianTask05b)
      printf("05b- Doc-Nurse review vitals and exam schedule\n")
      clearAlertAndSetExamTypeBasedOnTrend(alert, examType, trend)
      setExamTimeIfScheduled(examTime)
      putToken(clinicianGate08)
    }
  :: hasToken(clinicianTask06a) ->
    atomic {
      consumeToken(clinicianTask06a)
      printf("06a- Scheduler sets up urgent exam and informs patient\n")
      setExamTimeNow(examTime)
      putToken(clinicianGate09In1)
    }
  :: hasToken(clinicianTask06b) ->
    atomic {
      consumeToken(clinicianTask06b)
      printf("06b- Scheduler schedules routine exam and informs patient\n")
      if
      :: true -> setExamTimeScheduled(examTime)
      :: true
      fi
      putToken(clinicianGate09In2)
    }
  :: hasToken(clinicianWait00) ->
    atomic {
      consumeToken(clinicianWait00)
      putToken(clinicianTask05b)
    }
  od
}

proctype homeCareFlow() {
  do
  :: hasToken2Xor(clinicianEnd61, clinicianEndPtExpired) ->
    break
  :: hasToken(homeCareFlowEnd30) ->
    atomic {
      consumeToken(homeCareFlowEnd30)
    }
  :: hasToken2Xor(homeCareFlowGate00In0, homeCareFlowGate00In1) ->
    atomic {
      consumeToken(homeCareFlowGate00In0)
      consumeToken(homeCareFlowGate00In1)
      putToken(homeCareFlowTask03)
    }
  :: hasToken(homeCareFlowGate01) ->
    atomic {
      consumeToken(homeCareFlowGate01)
      if
      :: isExamTimeNow(examTime) ->
        putToken(homeCareFlowTask07)
      :: else ->
        putToken(homeCareFlowWait00)
      fi
    }
  :: hasToken(homeCareFlowRecv00)
    atomic {
      consumeToken(homeCareFlowRecv00)
      putToken(homeCareFlowTask00)
    }
  :: hasToken(homeCareFlowSend00) ->
    atomic {
      consumeToken(homeCareFlowSend00)
      putToken(clinicianRecv00)
      putToken(homeCareFlowEnd30)
    }
  :: hasToken(homeCareFlowTask00)
    atomic {
      consumeToken(homeCareFlowTask00)
      printf("Get Phware & install app\n")
      putToken(homeCareFlowGate00In0)
    }
  :: hasToken(homeCareFlowTask03) ->
    atomic {
      consumeToken(homeCareFlowTask03)
      printf("03- Pt-caregiver follow order to record vitals\n")
      setAndSendVitals(alert, trend)
      putToken(homeCareFlowGate01)
    }
  :: hasToken(homeCareFlowTask07) ->
    atomic {
      consumeToken(homeCareFlowTask07)
      printf("07- Pt prep for exam\n")
      putToken(homeCareFlowSend00)
    }
  :: hasToken(homeCareFlowWait00) ->
    atomic {
      consumeToken(homeCareFlowWait00)
      putToken(homeCareFlowGate00In1)
    }
  od
}

init {
  atomic {
    run clinician()
    run homeCareFlow()
  }
}

/*****************************************************************************/
/*                       CWP Verified: 30-Nov-2020-CWP                       */
/*****************************************************************************/

/*****************************************************************************/
/*                               CWP STates                                  */
/*****************************************************************************/
#define ICUState hasToken(clinicianTask03)
#define ptInAppropriateHomeCareState ((isPhware(patientStatus) || isNormal(patientStatus)) && isTrendGood(trend))
#define ptInElevatedRiskHomeCareState (isPhware(patientStatus) && isTrendBad(trend))
#define ptDischargedState (hasToken(clinicianEnd61) && isNormal(patientStatus))
#define ptExpiredState (hasToken(clinicianEndPtExpired) && isExpired(patientStatus))

#define transitioning     (! ICUState)                      \
                       && (! ptInAppropriateHomeCareState)  \
                       && (! ptInElevatedRiskHomeCareState) \
                       && (! ptDischargedState)             \
                       && (! ptExpiredState)                \

/*****************************************************************************/
/*                           Fairness Property                               */
/*****************************************************************************/
#define fair (eventually (ptDischargedState || ptExpiredState))
ltl fairPathExists {(always (! fair))}

/*****************************************************************************/
/*                Patient Discharged State Verification                      */
/*****************************************************************************/
ltl ptDischargedExists {(fair implies (always (! ptDischargedState)))}
ltl ptDischargedEdges {
  (
    fair implies (
      always (
        ptDischargedState implies (
          (always (ptDischargedState))
        )
      )
    )
  )
}

/*****************************************************************************/
/*                   Patient Expired State Verification                      */
/*****************************************************************************/
ltl ptExpiredExists {(fair implies (always (! ptExpiredState)))}
ltl ptExpiredEdges {
  (
    fair implies (
      always (
        ptExpiredState implies (
          (always (ptExpiredState))
        )
      )
    )
  )
}

/*****************************************************************************/
/*                        ICU State Verification                            */
/*****************************************************************************/
ltl ICUExists {(fair implies (always (! ICUState)))}
ltl ICUEdges {
  (
    fair implies (
      always (
        ICUState implies (
          (ICUState || transitioning) until (
                ptInAppropriateHomeCareState
            ||  ptExpiredState
            ||  ptDischargedState
          )
        )
      )
    )
  )
}


/*****************************************************************************/
/*              ptInAppropriateHomeCare State Verification                   */
/*****************************************************************************/
ltl ptInAppropriateHomeCareExists {(fair implies (always (! ptInAppropriateHomeCareState)))}
// Property fails with counter-example where exam sets the trend to bad and 
// orders ICU. This sequence creates a transition from 
// ptInAppropriateHomeCareEdges directly to ICUState that bypasses
// the required ptInElevatedRiskHomeCareState 
ltl ptInAppropriateHomeCareEdges {
  (
    fair implies (
      always (
        ptInAppropriateHomeCareState implies (
          (ptInAppropriateHomeCareState || transitioning) until (
                ptInElevatedRiskHomeCareState
            ||  ptDischargedState
          )
        )
      )
    )
  )
}

/*****************************************************************************/
/*              ptInElevatedRiskHomeCare State Verification                  */
/*****************************************************************************/
ltl ptInElevatedRiskHomeCareExists {(fair implies (always (! ptInElevatedRiskHomeCareState)))}
ltl ptInElevatedRiskHomeCareEdges {
  (
    fair implies (
      always (
        ptInElevatedRiskHomeCareState implies (
          (ptInElevatedRiskHomeCareState || transitioning) until (
                ptExpiredState
            ||  ICUState
            ||  ptInAppropriateHomeCareState
          )
        )
      )
    )
  )
}