//A single analysis is 6 hours, we need to set up channels such that the doctor 
//can take 2 messages before they HAVE
//to look at the dashboard, everything else is "paced" by this, 
//which really means that the homecare_f is the "pacer" it will always 
//be able to start an analysis, unless of course, its blocked from 
//doing so (perhaps the size two channel will be the "blocking" that
// happens?)

byte alert = 0
byte severity = 0 
byte next_exam = 0

byte num_analyses  = 0 // will need to be the number of analyses run
byte days = 0 // will need to be based on the number of analyses
byte time_normal_signs_started = 255 // will need? or could use one variable, once its set, never go back to "not" normal signs
byte normal_signs_time = 0 //
byte days_normal_signs = 0 // will need

chan analysis_to_doctor = [2] of { byte }

chan doctor_to_homecare = [1] of { byte }  // Revevalute these
chan homecare_to_doctor = [0] of { byte }


mtype = { start } 

mtype = { alert_true, alert_false }

mtype = { phware_ordered_02, patient_admitted, patient_discharged, reviewed_confirmed, reviewed_dismissed }
mtype = { severe_signs, normal_signs }
mtype = { urgent_exam, routine_exam, no_exam }

#define byte_max 255
#define time_slice 6

#define fourteen_days_passed (days >= 14)
#define normal_signs_discharge (fourteen_days_passed && days_normal_signs >= 3) 
#define five_days  5 * (24/time_slice)
#define one_day (24/time_slice)

active [1] proctype doctor_f() {
	printf("doctor_f\n")
Patient_pos: 
	printf("Patient test + for COVID19\n")
	byte patient = 0
    byte orders 
	goto Exam_Start 
Exam_loop:
	homecare_to_doctor?patient
Exam_Start:
	atomic {
		printf("01-Doctor or Nurse examine patient\n")
		num_analyses = 0 
		if
			:: true -> severity = severe_signs
				time_normal_signs_started = byte_max
			:: true -> severity = normal_signs // needs to change -- can't be unequivocally true
				if
					:: time_normal_signs_started == byte_max ->
						//time_normal_signs_started = num_analyses //not num_analyses
						printf("time_normal_signs_started = %d\n", time_normal_signs_started)
					:: else -> skip
				fi
		fi
		if
			:: severity == severe_signs ->
				orders = patient_admitted
				printf("Doctor admitted patient\n")
			:: severity == normal_signs -> 
				if
					:: normal_signs_discharge -> 
						orders = patient_discharged
						printf("14 + 3 days normal signs\n")
					:: else -> orders = phware_ordered_02 
						printf("Normal signs Doctor ordered phware\n")
				fi
		fi
		if 
			:: (orders == phware_ordered_02) -> 
				printf("02 Doctor orders home care with PHWARE and 4-7 day routine exam\n")
				next_exam = five_days
				run homecare_f()
			:: (orders == patient_admitted) ->
				printf("Doctor admitted patient for hospital or ICU care\n")
				goto end_47
			:: (orders == patient_discharged) ->
				printf("Patient discharged\n")
				goto end_61 		
		fi
	}
	byte exam_type = no_exam
	byte exam_time = byte_max // could remove, could replace with a "time" which would be number of analyses, if not removed, would need doctor/homecare time
	byte analysis_received = 0  // may not need anymore
Catch_AI_Packet: 
	atomic {
        printf("Seconds catch AI packet next_exam  = %d, num_analyses = %d\n", next_exam, num_analyses)
        if
            :: nfull(analysis_to_doctor) ->
                if
                    :: true -> goto Catch_AI_Packet // but how to block so that this only happens once, rather than infinitely with atomic block
                    :: true -> 
						printf("len 1 skipping\n")
						skip 
                fi
            :: full(analysis_to_doctor) -> skip 
        fi
Check_Analysis_Results:
        do
            :: analysis_to_doctor?[analysis_received] ->
                analysis_to_doctor?analysis_received
				printf("Doctor received analysis\n")
            :: else -> break
        od
        if // Should this go here? Need to check this after clearing the channel?
			:: next_exam == num_analyses -> 
                goto Exam_loop // Double check this
				printf("Going to Exam_loop\n")
			:: else -> skip
		fi
		printf("Doc-Nurse review alert/vitals trend\n")
		if
			:: alert == alert_true ->
				if
					:: true ->
						printf("Alert, Doctor orders urgent exam\n")
						exam_type = urgent_exam 
					:: true ->
						printf("Alert, Doctor orders no exam\n") 
						exam_type = no_exam 
				fi
		    :: alert == alert_false ->
				if
					:: true ->
                    	printf("No alert, Doctor orders urgent exam\n")
                        exam_type = urgent_exam
					:: true -> 
                        exam_type = no_exam
						printf("No alert, Doctor orders no exam\n") 
				fi
		fi
		if
			:: exam_type == urgent_exam -> 		
				next_exam = num_analyses + 1 
			:: exam_type == no_exam -> skip
		fi
		goto Catch_AI_Packet 
	}
end_61:
end_47: 	
	printf("end doctor_f\n")
}

proctype homecare_f() {
	atomic {
		printf("homecare_f\n")
Start_Homecare:
		printf("Get Phware & install app\n")
Check_next_exam:
    	// Figure out how to wait and then do something -- what do we "wait" on 
		printf("Homecare, next_exam = %d\n", next_exam)
		if 
			:: next_exam == num_analyses  ->
				goto Prep_Exam
			:: else -> skip 
		fi
		// HOW TO FIX running two analyses, and skipping an exam (basically)
		// Number of analyses sent by patient
		// Doctor based on the number of analyses sent by patient, not number of analyses sent by the analysis
		nfull(analysis_to_doctor) && _nr_pr < 4 -> skip 
		run analysis_f()
		goto Check_next_exam //when do we do this? only if its not exam_time
Prep_Exam:
		printf("07- Patient prep exam\n")
		homecare_to_doctor!start 
	}
end_30:
	printf("end homecare_f\n")
}

proctype analysis_f() {
	atomic {
		printf("analysis_f\n")
Start225:	
		if
			:: true ->
					alert = alert_true
			:: true ->
					alert = alert_false
		fi
		analysis_to_doctor!start
		printf("Analysis sent results to doctor\n")
		num_analyses = num_analyses + 1
end_361:
	printf("end analysis_f\n")
	}
}