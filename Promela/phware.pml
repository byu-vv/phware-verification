byte alert = 0
byte severity = 0 
byte next_routine_exam = 0

byte time = 0 
byte doctor_time = 0
byte homecare_time = 0 
byte days = 0 
byte time_normal_signs_started = 255 
byte normal_signs_time = 0
byte days_normal_signs = 0

chan doctor_to_homecare = [1] of { byte }  
chan homecare_to_doctor = [0] of { byte }
chan analysis_to_doctor = [1] of { byte }
chan homecare_to_analysis = [0] of { byte }

mtype = { start, stop } 

byte doctor_pos = start
byte homecare_pos = 0
byte analysis_pos = 0

mtype = { alert_true, alert_false }

mtype = { doctor_01, phware_ordered_02, patient_admitted, patient_discharged, reviewed_confirmed, reviewed_dismissed, doctor_end }
mtype = { severe_signs, normal_signs }
mtype = { doctor_nurse_05, schedule_exam_06 }
mtype = { urgent_exam, routine_exam, no_exam  }

mtype = { install_phware, patient_record_03, patient_exam_07 }
mtype = { analyze }

// #define one_day 4 //24
// #define four_days 16 //96
// #define five_days 20 //120
// #define six_days 24 //144
// #define seven_days 28 //168
//#define time_slice 2 //The number of hours to slice each day into

#define one_day 24 
#define four_days 96 
#define five_days 120 
#define six_days 144 
#define seven_days 168 

#define byte_max 255

#define fourteen_days_passed (days >= 14)
#define normal_signs_discharge (fourteen_days_passed && days_normal_signs >= 3) 
#define routine_exam_date (time == next_routine_exam - one_day) // What happens if next_routine_exam is 0, it will be truncated to a correct value...
#define doc_hour_delay (doctor_time == 1)
#define one_day_passed time % one_day == 0
#define inc_days days = days + 1 
#define inc_normal_signs_time normal_signs_time = normal_signs_time + 1
#define inc_days_normal_signs days_normal_signs = days_normal_signs + 1
#define now time + 1
#define inc_time(time_) time_ = time_ + 1
#define inc_wrap_time time = (time + 1) % seven_days
#define inc_days_normal_signs_cond fourteen_days_passed && (normal_signs_time % one_day == 0)
#define inc_normal_signs_time_cond fourteen_days_passed && normal_signs_time != byte_max 
#define wait_one_hour_homecare (homecare_time == tmp + 1)
// #define reset_homecare_time_cond (homecare_time == 6)
// #define reset_homecare_time (homecare_time = 0)
 
proctype timer() {
Begin_timer:	
	atomic {
		if
			:: one_day_passed && !fourteen_days_passed ->
				inc_days
				printf("time = %d, days = %d\n", time, days) 
			:: else -> skip
		fi
		if 
			:: inc_normal_signs_time_cond ->
				inc_normal_signs_time
				printf("normal_signs_time = %d\n", normal_signs_time)
			:: else -> skip
		fi
		if
			:: inc_days_normal_signs_cond ->
				inc_days_normal_signs
				printf("days_normal_signs increase = %d\n", days_normal_signs)
			:: else -> skip
		fi
		if
			:: (normal_signs_discharge || doctor_pos == doctor_end) ->
				printf("Normal signs or patient admitted/discharged\n")
				goto end_timer
			:: else -> skip 
		fi
		inc_wrap_time
		inc_time(doctor_time)
		inc_time(homecare_time)
		timeout
		goto Begin_timer
	}
end_timer:

}

proctype doctor_f() {
	printf("doctor_f\n")
Patient_pos: 
	printf("Patient test + for COVID19\n")
	byte patient = 0
	goto Exam_Start 
Exam_loop:
	homecare_to_doctor?patient 
Exam_Start:
	doctor_pos = doctor_01 
	atomic {
		printf("01-Doctor or Nurse examine patient\n")
		if
			:: true -> severity = severe_signs
				days_normal_signs = 0
				time_normal_signs_started = byte_max
			:: true -> severity = normal_signs
				if
					:: time_normal_signs_started == byte_max ->
						time_normal_signs_started = time
						printf("time_normal_signs_started = %d\n", time_normal_signs_started)
					:: else -> skip
				fi
		fi
		if
			:: severity == severe_signs ->
				if
					:: true -> 
						doctor_pos = phware_ordered_02
						printf("Severe signs Doctor ordered phware\n")
					:: true -> 
						doctor_pos = patient_admitted
						printf("Doctor admitted patient\n")
				fi
			:: severity == normal_signs -> 
				if
					:: normal_signs_discharge -> 
						doctor_pos = patient_discharged
						printf("14 + 3 days normal signs\n")
					:: else -> doctor_pos = phware_ordered_02 
						printf("Normal signs Doctor ordered phware\n")
				fi
		fi
		if 
			:: (doctor_pos == phware_ordered_02) -> 
				printf("02 Doctor orders home care with PHWARE and 4-7 day routine exam\n")
				true -> next_routine_exam = (time + five_days) % seven_days
				run homecare_f()

			:: (doctor_pos == patient_admitted) ->
				printf("Doctor admitted patient for hospital or ICU care\n")
				goto end_47
			:: (doctor_pos == patient_discharged) ->
				printf("Patient discharged\n")
				goto end_61 		
		fi
	}
	byte exam_type = 0
	byte exam_time = byte_max
	byte analysis_received = 0  
Catch_AI_Packet:
	atomic {
		doc_hour_delay
		doctor_time = 0
		printf("Seconds catch AI packet time = %d\n", time) 
		if
			:: analysis_to_doctor?[analysis_received] ->
				printf("Doctor received analysis\n")
				analysis_to_doctor?analysis_received
			:: else -> skip
		fi
		if
			:: exam_time == time -> goto Exam_loop
				printf("Going to Exam_loop\n")
			:: else -> skip
		fi
		if
			:: analysis_received == start -> skip
			:: else -> goto Catch_AI_Packet 
		fi
		doctor_pos = doctor_nurse_05 
	}
	(doctor_pos == doctor_nurse_05) ->
	atomic {
		printf("Doc-Nurse review alert/vitals trend\n")
		if
			:: alert == alert_true ->
				byte reviewed
				if
					:: true -> reviewed = reviewed_confirmed
					:: true -> reviewed = reviewed_dismissed
				fi
				if
					:: reviewed == reviewed_confirmed ->
						printf("Alert, Doctor orders urgent exam\n")
Trend_gt_care1:
						exam_type = urgent_exam 
					:: reviewed == reviewed_dismissed ->
						printf("Alert, Doctor orders no exam\n") 
						exam_type = no_exam 
						goto Catch_AI_Packet 
				fi
		    :: alert == alert_false ->
				if
					:: routine_exam_date -> 
						printf("No alert, Docotor orders routine exam\n")
						exam_type = routine_exam
					:: true -> exam_type = urgent_exam
						printf("No alert, Doctor orders urgent exam\n")
					:: true -> exam_type = no_exam
						printf("No alert, Doctor orders no exam\n") 
				fi
				if
					:: exam_type == routine_exam -> skip
					:: exam_type == urgent_exam -> skip
					:: exam_type == no_exam -> goto Catch_AI_Packet 
				fi
		fi
		doctor_pos = schedule_exam_06	
		(doctor_pos == schedule_exam_06) ->
		printf("Sending exam_time to homecare\n")
		if
			:: exam_type == urgent_exam ->
				exam_time = now 
			:: exam_type == routine_exam ->
				exam_time = next_routine_exam 
		fi
		doctor_to_homecare!exam_time
		printf("sent exam_time = %d to homecare\n", exam_time)
		goto Catch_AI_Packet 
	}
end_61:
end_47: 	
	doctor_pos = doctor_end 
	printf("end doctor_f\n")
}

proctype homecare_f() {
	printf("homecare_f\n")
	byte exam_time = byte_max
	byte tmp = byte_max
Start_Homecare:
	printf("Get Phware & install app\n")
	homecare_pos = patient_record_03
	(homecare_pos == patient_record_03) ->
Check_exam_time:
	atomic {
		printf("03- Pt-caregiver follow order to record vitals\n")
		if
			:: doctor_to_homecare?[exam_time] ->
				doctor_to_homecare?exam_time 
				printf("Patient received exam_time = %d\n", exam_time)
			:: else -> skip
				printf("Patient did not receive an exam time\n")
		fi
		if 
			:: exam_time == time ->
				homecare_pos = patient_exam_07
				goto Prep_Exam
			:: else -> 
		fi
		if 
			:: homecare_time == 0 -> 
				run analysis_f()
				homecare_to_analysis!start
			:: else -> skip 
		fi
		tmp = homecare_time
Homecare_wait:
		wait_one_hour_homecare
		// reset homecare_time 
		if
			:: homecare_time == 6 ->
				homecare_time = 0
			:: else -> skip 
		fi
		goto Check_exam_time
	}
Prep_Exam:
	atomic {
		(homecare_pos == patient_exam_07) ->
		printf("07- Patient prep exam\n")
		homecare_to_doctor!start 
	}
end_30:
	printf("end homecare_f\n")
}

proctype analysis_f() {
	printf("analysis_f\n")
	byte start_analysis = 0 
Start225:	
	atomic { 
		homecare_to_analysis?start_analysis
		analysis_pos = analyze
		if
			:: true ->
					alert = alert_true
			:: true ->
					alert = alert_false
		fi
		analysis_to_doctor!start
		printf("Analysis sent results to doctor\n")
	}
End361:
	printf("end analysis_f\n")
}

init {
	run doctor_f()
	run timer()
}

//Keith's State Machine
// never {
// 	true;
// 	do
// 		:: doctor_f@[Exam_Start] || doctor_f@[Exam_loop]
// 		:: doctor_f@[Catch_AI_Packet]
// 		:: homecare_f@[Homecare_wait]

// 	od


// }

