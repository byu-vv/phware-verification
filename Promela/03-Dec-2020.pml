chan heartBeat = [2] of {bool}

/*****************************************************************************/
/*                         Clinician Flow Places                             */
/*****************************************************************************/
bit clinicianEnd61 = 0
bit clinicianEndPtExpired = 0
bit clinicianGate00In0 = 0
bit clinicianGate00In1 = 0
bit clinicianGate00In2 = 0
bit clinicianGate01In0 = 0
bit clinicianGate01In1 = 0
bit clinicianGate02In0 = 0
bit clinicianGate02In1 = 0
bit clinicianGate02In2 = 0
bit clinicianGate03 = 0
bit clinicianGate04 = 0
bit clinicianGate05 = 0
bit clinicianGate06 = 0
bit clinicianGate07 = 0
bit clinicianGate08 = 0
bit clinicianGate09In0 = 0
bit clinicianGate09In1 = 0
bit clinicianGate09In2 = 0
bit clinicianGate09In3 = 0
bit clinicianGate10In0 = 0
bit clinicianGate10In1 = 0
bit clinicianGate11In0 = 0
bit clinicianGate11In1 = 0
bit clinicianRecv00 = 0
bit clinicianRecv01 = 0
bit clinicianRecv01Vitals = 0
bit clinicianTask01 = 0
bit clinicianTask02 = 0
bit clinicianTask03 = 0
bit clinicianTask05a = 0
bit clinicianTask05b = 0
bit clinicianTask06a = 0
bit clinicianTask06b = 0
bit clinicianWait00 = 0

/*****************************************************************************/
/*                         Home Care Flow Places                             */
/*****************************************************************************/
bit homeCareFlowEnd30 = 0
bit homeCareFlowEnd1057 = 0
bit homeCareFlowGate00In0 = 0
bit homeCareFlowGate00In1 = 0
bit homeCareFlowGate01 = 0
bit homeCareFlowGate02 = 0
bit homeCareFlowTask00 = 0
bit homeCareFlowTask03 = 0
bit homeCareFlowTask07 = 0
bit homeCareFlowSend00 = 0
bit homeCareFlowRecv00 = 0
bit homeCareFlowWait00 = 0

/*****************************************************************************/
/*                             Token Management                              */
/*****************************************************************************/
#define hasToken(place) (place != 0)

#define putToken(place) place = 1

#define consumeToken(place) place = 0

#define hasToken2Xor(place0, place1) \
  ((place0 != 0 && place1 == 0) || (place0 == 0 && place1 != 0))


#define hasToken3Xor(place0, place1, place2)         \
  (                                                  \
       (place0 != 0 && place1 == 0 && place2 == 0)   \
    || (place0 == 0 && place1 != 0 && place2 == 0)   \
    || (place0 == 0 && place1 == 0 && place2 != 0)   \
  )

#define hasToken4Xor(place0, place1, place2, place3)                \
  (                                                                 \
       (place0 != 0 && place1 == 0 && place2 == 0 && place3 == 0)   \
    || (place0 == 0 && place1 != 0 && place2 == 0 && place3 == 0)   \
    || (place0 == 0 && place1 == 0 && place2 != 0 && place3 == 0)   \
    || (place0 == 0 && place1 == 0 && place2 == 0 && place3 != 0)   \
  )

#define hasToken2And(place0, place1) (place0 != 0 && place1 != 0)

/*****************************************************************************/
/*                            Doctors Orders                                 */
/*****************************************************************************/
mtype = {homeCare, hospital, discharge}
mtype orders = homeCare

#define isHomeCare(orders) (orders == homeCare)

#define setHomeCare(orders) orders = homeCare

#define isHospital(orders) (orders == hospital)

#define setHospital(orders) orders = hospital

#define isDischarge(orders) (orders == discharge)

#define setDischarge(orders) orders = discharge

inline logOrders(orders) {
  printf("orders = %e", orders)
}

/*****************************************************************************/
/*                            Patient Severity                               */
/*****************************************************************************/
mtype = {withinCareCapability, outsideCareCapability, requiresHospital, expired}
mtype severity = withinCareCapability
mtype previousSeverity = withinCareCapability

#define isWithinCareCapability(severity) (severity == withinCareCapability)

#define setWithinCareCapability(severity) previousSeverity = severity ; severity = withinCareCapability

#define isOutsideCareCapability(severity) (severity == outsideCareCapability)

#define setOutsideCareCapability(severity) previousSeverity = severity ; severity = outsideCareCapability

#define isRequiresHospital(severity) (severity == requiresHospital)

#define setRequiresHospital(severity) previousSeverity = severity ; severity = requiresHospital

#define isExpired(severity) (severity == expired)

#define setExpired(severity) previousSeverity = severity ; severity = expired

inline logSeverity(severity) {
  printf("severity = %e", severity)
}

/*****************************************************************************/
/*                         Patient Severity Trend                            */
/*****************************************************************************/
mtype = {positive, negative}
mtype trend = positive

#define isPositive(trend) (trend == positive)

#define setPositive(trend) trend = positive

#define isNegative(trend) (trend == negative)

#define setNegative(trend) trend = negative

inline logTrend(trend) {
  printf("trend = %e", trend)
}

/*****************************************************************************/
/*                                   Alert                                   */
/*****************************************************************************/
bool alert = false

#define isAlert(alert) (alert == true)

#define setAlert(alert) alert = true

#define clearAlert(alert) alert = false

inline logAlert(alert) {
  printf("alert = %d", alert)
}

/*****************************************************************************/
/*                                Exam Type                                  */
/*****************************************************************************/
mtype = {routine, urgent}
mtype examType = routine

#define isExamTypeRoutine(type) (type == routine)

#define setExamTypeRoutine(type) type = routine

#define isExamTypeUrgent(type) (type == urgent)

#define setExamTypeUrgent(type) type = urgent

inline logExamType(type) {
  printf("examType = %e", type)
}

/*****************************************************************************/
/*                                Exam Time                                  */
/*****************************************************************************/
mtype = {now, unscheduled, scheduled}
mtype examTime = unscheduled

#define isExamTimeNow(time) (time == now)

#define setExamTimeNow(time) time = now

#define isExamTimeUnscheduled(time) (time == unscheduled)

#define setExamTimeUnscheduled(time) time = unscheduled

#define isExamTimeScheduled(time) (time == scheduled)

#define setExamTimeScheduled(time) time = scheduled

inline logExamTime(time) {
  printf("examTime = %e", time)
}

/*****************************************************************************/
/*                     Behavior Model for Severity Trend                     */
/*****************************************************************************/
inline updateSeverityTrendAndAlert(alert, trend, severity) {
  if
  :: isAlert(alert)
  :: else -> 
    if
    :: isOutsideCareCapability(severity) -> 
      setNegative(trend)
      setAlert(alert)
    :: true -> 
      setNegative(trend)
    :: true -> 
      setPositive(trend)
    fi
  fi
  logAlert(alert)
  printf(" ")
  logTrend(trend)
  printf("\n")
}

/*****************************************************************************/
/*                    Behavior Model for Patient Severity                    */
/*****************************************************************************/
inline updatePatientSeverity(trend, severity) {
  if
  :: isWithinCareCapability(severity) ->
    if
    :: true
    :: isNegative(trend) ->
      setOutsideCareCapability(severity)
    fi
  :: isOutsideCareCapability(severity)
    if
    :: true
    :: isPositive(trend) ->
      setWithinCareCapability(severity)
    :: isNegative(trend) ->
      setRequiresHospital(severity)
    fi
  :: isRequiresHospital(severity) ->
      setWithinCareCapability(severity)
  :: else
  fi
  logSeverity(severity)
  printf("\n");
}

inline updatePatientMortality(trend, severity) {
  if
  :: true
  :: (isNegative(trend) && isOutsideCareCapability(severity)) ->
    setExpired(severity)
  :: (isRequiresHospital(severity)) ->
    setExpired(severity)
  fi
}

/*****************************************************************************/
/*                     Behavior Model for Doctor Orders                      */
/*****************************************************************************/
inline updateDoctorOrders(severity, orders) {
  if
  :: isWithinCareCapability(severity) ->
    if
    :: true ->
      setHomeCare(orders)
    :: isWithinCareCapability(previousSeverity) ->
      setDischarge(orders)
    fi
  :: isRequiresHospital(severity) -> 
    setHospital(orders)
  :: else ->
    setHomeCare(orders)
  fi
  logOrders(orders)
  printf("\n")
}

/*****************************************************************************/
/*                       Behavior Model for Exam Type                        */
/*****************************************************************************/
inline updateExamType(alert, trend, severity, examType) {
  clearAlert(alert)
  if
  :: (isExamTypeRoutine(examType) && isOutsideCareCapability(severity)) ->
    setExamTypeUrgent(examType)
    logExamType(examType)
    printf("\n")
  :: (isExamTypeRoutine(examType) && isOutsideCareCapability(severity))
  :: else
  fi
}

/*****************************************************************************/
/*                       Behavior Model for Exam Time                        */
/*****************************************************************************/
inline setExamTimeIfScheduled(time) {
  if
  :: isExamTimeScheduled(time) ->
    setExamTimeNow(time)
    logExamTime(time)
    printf("\n")
  :: true
  fi
}

proctype clinician() {
  putToken(clinicianGate00In2)
  putToken(clinicianGate02In1)
  do
  :: hasToken(clinicianEnd61) ->
    atomic {
      break
    }
  :: hasToken(clinicianEndPtExpired) ->
    atomic {
      break
    }
  :: hasToken(homeCareFlowEnd1057) ->
    atomic {
      break
    }
  :: hasToken3Xor(clinicianGate00In0, clinicianGate00In1, clinicianGate00In2) ->
    atomic {
      consumeToken(clinicianGate00In0)
      consumeToken(clinicianGate00In1)
      consumeToken(clinicianGate00In2)
      putToken(clinicianGate01In1)
    }
  :: hasToken2And(clinicianGate01In0, clinicianGate01In1) ->
    atomic {
      consumeToken(clinicianGate01In0)
      consumeToken(clinicianGate01In1)
      putToken(clinicianTask01)
    }
  :: hasToken3Xor(clinicianGate02In0, clinicianGate02In1, clinicianGate02In2) ->
    atomic {
      consumeToken(clinicianGate02In0)
      consumeToken(clinicianGate02In1)
      consumeToken(clinicianGate02In2)
      putToken(clinicianGate01In0)
    }
  :: hasToken(clinicianGate03) ->
    atomic {
      consumeToken(clinicianGate03)
      if
      :: isHospital(orders) ->
        putToken(clinicianTask03)
      :: isDischarge(orders) ->
        putToken(clinicianEnd61)
      :: isHomeCare(orders) ->
        putToken(clinicianTask02)
      fi
    }
  :: hasToken(clinicianGate04) ->
    atomic {
      consumeToken(clinicianGate04)
      if
      :: isExpired(severity) ->
        putToken(clinicianEndPtExpired)
      :: else ->
        putToken(clinicianGate05)
      fi
    }
  :: hasToken(clinicianGate05) ->
    atomic {
      consumeToken(clinicianGate05)
      putToken(clinicianGate02In2)
      putToken(clinicianGate00In0)
    }
  :: hasToken(clinicianGate06) ->
    atomic {
      consumeToken(clinicianGate06)
      if
      :: isAlert(alert) ->
        putToken(clinicianTask05a)
      :: else ->
        putToken(clinicianWait00)
      fi
    }
  :: hasToken(clinicianGate07) ->
    atomic {
      consumeToken(clinicianGate07)
      if 
      :: isExamTimeNow(examTime) ->
        putToken(clinicianGate09In3)
      :: else ->
        if
        :: isExamTypeUrgent(examType) ->
          putToken(clinicianGate10In1)
        :: isExamTypeRoutine(examType) && isExamTimeUnscheduled(examTime) ->
          putToken(clinicianGate11In0)
        :: else ->
          putToken(clinicianRecv01)
        fi
      fi
    }
  :: hasToken(clinicianGate08) ->
    atomic {
      consumeToken(clinicianGate08)
      if 
      :: isExamTimeNow(examTime) ->
        putToken(clinicianGate09In0)
      :: else ->
        if
        :: isExamTypeUrgent(examType) ->
          putToken(clinicianGate10In0)
        :: isExamTypeRoutine(examType) && isExamTimeUnscheduled(examTime) ->
          putToken(clinicianGate11In1)
        :: else ->
          putToken(clinicianRecv01)
        fi
      fi
    }
  :: hasToken4Xor(clinicianGate09In0, clinicianGate09In1, clinicianGate09In2, clinicianGate09In3) ->
    atomic {
      consumeToken(clinicianGate09In0)
      consumeToken(clinicianGate09In1)
      consumeToken(clinicianGate09In2)
      consumeToken(clinicianGate09In3)
      if
      :: isExamTimeNow(examTime) ->
        putToken(clinicianGate02In0)
      :: else -> 
        putToken(clinicianRecv01)
      fi
    }
  :: hasToken2Xor(clinicianGate10In0, clinicianGate10In1) ->
    atomic {
      consumeToken(clinicianGate10In0)
      consumeToken(clinicianGate10In1)
      putToken(clinicianTask06a)
    }
  :: hasToken2Xor(clinicianGate11In0, clinicianGate11In1) ->
    atomic {
      consumeToken(clinicianGate11In0)
      consumeToken(clinicianGate11In1)
      putToken(clinicianTask06b)
    }
  :: hasToken(clinicianRecv00) ->
    atomic {
      consumeToken(clinicianRecv00)
      putToken(clinicianGate00In1)
    }
   :: hasToken2And(clinicianRecv01, clinicianRecv01Vitals) ->
    atomic {
      consumeToken(clinicianRecv01)
      consumeToken(clinicianRecv01Vitals)
      putToken(clinicianGate06)
    }
  :: hasToken(clinicianTask01) ->
    atomic {
      consumeToken(clinicianTask01)
      consumeToken(clinicianRecv01Vitals)
      printf("01- Doctor or Nurse examine pt\n")
      updatePatientSeverity(trend, severity)
      updateDoctorOrders(severity, orders)
      setExamTimeUnscheduled(examTime)
      setExamTypeRoutine(examType)
      putToken(clinicianGate03)
    }
  :: hasToken(clinicianTask02) ->
    atomic {
      consumeToken(clinicianTask02)
      printf("02- Doctor orders home care with PHWARE and 4-7 day routine exam\n")
      setExamTypeRoutine(examType)
      putToken(clinicianRecv01)
      putToken(homeCareFlowRecv00)
    }
  :: hasToken(clinicianTask03) ->
    atomic {
      consumeToken(clinicianTask03)
      printf("Doctor admit pt for hospital or ICU care\n")
      updatePatientMortality(trend, severity)
      putToken(clinicianGate04)
    }
  :: hasToken(clinicianTask05a) ->
    atomic {
      consumeToken(clinicianTask05a)
      printf("05a- Doc-Nurse review alert, vitals and exam schedule\n")
      updateExamType(alert, trend, severity, examType)
      setExamTimeIfScheduled(examTime)
      putToken(clinicianGate07)
    }
  :: hasToken(clinicianTask05b) ->
    atomic {
      consumeToken(clinicianTask05b)
      printf("05b- Doc-Nurse review vitals and exam schedule\n")
      updateExamType(alert, trend, severity, examType)
      setExamTimeIfScheduled(examTime)
      putToken(clinicianGate08)
    }
  :: hasToken(clinicianTask06a) ->
    atomic {
      consumeToken(clinicianTask06a)
      printf("06a- Scheduler sets up urgent exam and informs patient\n")
      setExamTimeNow(examTime)
      putToken(clinicianGate09In1)
    }
  :: hasToken(clinicianTask06b) ->
    atomic {
      consumeToken(clinicianTask06b)
      printf("06b- Scheduler schedules routine exam and informs patient\n")
      if
      :: true -> setExamTimeScheduled(examTime)
      :: true
      fi
      putToken(clinicianGate09In2)
    }
  :: hasToken(clinicianWait00) ->
    atomic {
      consumeToken(clinicianWait00)
      putToken(clinicianTask05b)
    }
  od
}

proctype homeCareFlow() {
  do
  :: (hasToken(clinicianEnd61) || hasToken(clinicianEndPtExpired)) ->
    atomic {
      break
    }
  :: hasToken(homeCareFlowEnd30) ->
    atomic {
      consumeToken(homeCareFlowEnd30)
    }
  :: hasToken(homeCareFlowEnd1057) ->
    atomic {
      break
    }
  :: hasToken2Xor(homeCareFlowGate00In0, homeCareFlowGate00In1) ->
    atomic {
      consumeToken(homeCareFlowGate00In0)
      consumeToken(homeCareFlowGate00In1)
      putToken(homeCareFlowTask03)
    }
  :: hasToken(homeCareFlowGate01) ->
    atomic {
      consumeToken(homeCareFlowGate01)
      if
      :: isExamTimeNow(examTime) ->
        putToken(homeCareFlowTask07)
      :: else ->
        putToken(homeCareFlowWait00)
      fi
    }
  :: hasToken(homeCareFlowGate02) ->
    atomic {
      consumeToken(homeCareFlowGate02)
      if
      :: isExpired(severity) ->
        putToken(homeCareFlowEnd1057)
      :: else ->
        putToken(homeCareFlowGate01)
      fi
    }
  :: hasToken(homeCareFlowRecv00)
    atomic {
      consumeToken(homeCareFlowRecv00)
      putToken(homeCareFlowTask00)
    }
  :: hasToken(homeCareFlowSend00) ->
    atomic {
      consumeToken(homeCareFlowSend00)
      putToken(clinicianRecv00)
      putToken(homeCareFlowEnd30)
    }
  :: hasToken(homeCareFlowTask00)
    atomic {
      consumeToken(homeCareFlowTask00)
      printf("Get Phware & install app\n")
      putToken(homeCareFlowGate00In0)
    }
  :: hasToken(homeCareFlowTask03) ->
    atomic {
      consumeToken(homeCareFlowTask03)
      printf("03- Pt-caregiver follow order to record vitals\n")
      updateSeverityTrendAndAlert(alert, trend, severity)
      updatePatientMortality(trend, severity)
      putToken(clinicianRecv01Vitals)
      putToken(homeCareFlowGate02)
    }
  :: hasToken(homeCareFlowTask07) ->
    atomic {
      consumeToken(homeCareFlowTask07)
      printf("07- Pt prep for exam\n")
      putToken(homeCareFlowSend00)
    }
  :: hasToken(homeCareFlowWait00) ->
    atomic {
      consumeToken(homeCareFlowWait00)
      putToken(homeCareFlowGate00In1)
    }
  od
}

init {
  atomic {
    run clinician()
    run homeCareFlow()
  }
}

/*****************************************************************************/
/*                       CWP Verified: 02-Dec-2020-CWP                       */
/*****************************************************************************/

/*****************************************************************************/
/*                               CWP STates                                  */
/*****************************************************************************/
#define ICUState (hasToken(clinicianTask03) && isHospital(orders))
#define ptInAppropriateHomeCareState (isHomeCare(orders) && isWithinCareCapability(severity))
#define ptInElevatedRiskHomeCareState (isHomeCare(orders) && isOutsideCareCapability(severity))
#define ptDischargedState (hasToken(clinicianEnd61) && isDischarge(orders))
#define ptExpiredState ((hasToken(clinicianEndPtExpired) || hasToken(homeCareFlowEnd1057)) && isExpired(severity))

#define transitioning     (! ICUState)                      \
                       && (! ptInAppropriateHomeCareState)  \
                       && (! ptInElevatedRiskHomeCareState) \
                       && (! ptDischargedState)             \
                       && (! ptExpiredState)                \
                      
/*****************************************************************************/
/*                           Fairness Property                               */
/*****************************************************************************/
#define fair                                                                  \
  (                                                                           \
    (                                                                         \
      (eventually (ptDischargedState || ptExpiredState))                      \
    )                                                                         \
  )
ltl fairPathExists {(always (! fair))}

/*****************************************************************************/
/*                Patient Discharged State Verification                      */
/*****************************************************************************/
ltl ptDischargedExists {(fair implies (always (! ptDischargedState)))}
ltl ptDischargedEdges {
  (
    fair implies (
      always (
        ptDischargedState implies (
          (always (ptDischargedState))
        )
      )
    )
  )
}

/*****************************************************************************/
/*                   Patient Expired State Verification                      */
/*****************************************************************************/
ltl ptExpiredExists {(fair implies (always (! ptExpiredState)))}
ltl ptExpiredEdges {
  (
    fair implies (
      always (
        ptExpiredState implies (
          (always (ptExpiredState))
        )
      )
    )
  )
}

/*****************************************************************************/
/*                        ICU State Verification                            */
/*****************************************************************************/
ltl ICUExists {(fair implies (always (! ICUState)))}
ltl ICUEdges {
  (
    fair implies (
      always (
        ICUState implies (
          (ICUState || transitioning) until (
                ptInAppropriateHomeCareState
            ||  ptExpiredState
            ||  ptDischargedState
          )
        )
      )
    )
  )
}


/*****************************************************************************/
/*              ptInAppropriateHomeCare State Verification                   */
/*****************************************************************************/
ltl ptInAppropriateHomeCareExists {(fair implies (always (! ptInAppropriateHomeCareState)))}
ltl ptInAppropriateHomeCareEdges {
  (
    fair implies (
      always (
        ptInAppropriateHomeCareState implies (
          (ptInAppropriateHomeCareState || transitioning) until (
                ptInElevatedRiskHomeCareState
            ||  ptDischargedState
          )
        )
      )
    )
  )
}

/*****************************************************************************/
/*              ptInElevatedRiskHomeCare State Verification                  */
/*****************************************************************************/
ltl ptInElevatedRiskHomeCareExists {(fair implies (always (! ptInElevatedRiskHomeCareState)))}
ltl ptInElevatedRiskHomeCareEdges {
  (
    fair implies (
      always (
        ptInElevatedRiskHomeCareState implies (
          (ptInElevatedRiskHomeCareState || transitioning) until (
                ptExpiredState
            ||  ICUState
            ||  ptInAppropriateHomeCareState
          )
        )
      )
    )
  )
}